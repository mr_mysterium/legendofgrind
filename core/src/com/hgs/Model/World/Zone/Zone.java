package com.hgs.Model.World.Zone;

import com.hgs.Model.World.LootTable;

public abstract class Zone {
    String name;
    LootTable loot;
    int steps;
    float moneyreward;
    float xpreward;

    public Zone(String name,LootTable loot,int steps,float moneyreward,float xpreward){
        this.name=name;
        this.loot=loot;
        this.steps=steps;
        this.moneyreward=moneyreward;
        this.xpreward=xpreward;
    }

    public String getName() {
        return name;
    }

    public LootTable getLoot() {
        return loot;
    }

    public int getSteps() {
        return steps;
    }

    public float getMoneyreward() {
        return moneyreward;
    }

    public float getXpreward() {
        return xpreward;
    }
}
