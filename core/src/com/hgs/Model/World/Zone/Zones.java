package com.hgs.Model.World.Zone;

import com.badlogic.gdx.utils.Array;

public enum  Zones {
   Hills(new Hills()), Forest(new Forest()),;

    private final Zone zone;

    Zones(Zone zone) {
        this.zone = zone;

    }

    public Zone getZone() {
        return zone;
    }

    public static Array<String>  getzones(){
        Array<String>zones=new Array<String>();
        for (Zones  zone : Zones.values()) {
            zones.add(zone.name());
        }
        return zones;
    }
}
