package com.hgs.Model.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.hgs.Model.LOG;
import com.hgs.Model.Scenes.Scene;

public class GameScreen implements Screen {
    final LOG log;
    final Stage stage;
    float widthratio;
    float heightratio;
    private ShapeRenderer shapeRenderer;
    private Rectangle rectangle;
    final InputMultiplexer multiplexer;


    public GameScreen(LOG log){
        this.log =log;
        ScreenViewport screenViewport = new ScreenViewport();
        stage = new Stage(screenViewport);
        for (Scene scene:log.scenes)stage.addActor(scene.getTable());
        multiplexer = new InputMultiplexer(stage);
        Gdx.input.setInputProcessor(multiplexer);
        stage.setDebugAll(true);
    }



    @Override
    public void show() {
        widthratio = (float) LOG.WIDTH/ LOG.oWIDTH;
        heightratio = (float) LOG.HEIGHT/ LOG.oHEIGHT;
    }



    @Override
    public void render(float delta) {
        Gdx.graphics.setTitle(""+Gdx.graphics.getFramesPerSecond());
        Gdx.gl.glClearColor(0/255f, 0/255f, 0/255f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        for (Scene scene:log.scenes)scene.render(delta,stage.getBatch());
        stage.act(delta);
//        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }



    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void dispose() {
        stage.dispose();
        LOG.batch.dispose();
    }


    public void updateview() {
        stage.getViewport().update(LOG.WIDTH, LOG.HEIGHT);
        stage.getCamera().position.set(LOG.WIDTH/2, LOG.HEIGHT/2,0);
        stage.getCamera().update();
        widthratio = (float) LOG.WIDTH/ LOG.oWIDTH;
        heightratio = (float) LOG.HEIGHT/ LOG.oHEIGHT;
        for (Scene scene:log.scenes)scene.scale(widthratio<heightratio?widthratio:heightratio);
    }

    public float getratio(){
        return widthratio<heightratio?widthratio:heightratio;
    }

    public Stage getStage() {
        return stage;
    }

    public ShapeRenderer getShapeRenderer() {
        return shapeRenderer;
    }
}
