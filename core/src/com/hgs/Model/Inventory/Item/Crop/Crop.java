package com.hgs.Model.Inventory.Item.Crop;

import com.badlogic.gdx.Gdx;
import com.hgs.Model.Inventory.Item.Item;
import com.hgs.Model.Inventory.Item.Seed.Seed;

public abstract class Crop extends Item {

    public Crop(String name, float growthpoints, float moneyvalue, float xpvalue){
        super(name);
        getData().putString("category","Crop");
        getData().putFloat("growthpoints",growthpoints);
        getData().putDouble("moneyvalue",moneyvalue);
        getData().putDouble("xpvalue",xpvalue);
        getData().putFloat("growth",0);
        getData().putFloat("progress",0);
        getData().putboolean("grown",false);

    }

    public Crop(Crop crop) {
        super(crop);
    }

    public float grow(float amount){
            float growth=getData().getFloat("growth",0);
            float growthpoints=getData().getFloat("growthpoints",0);
            float progress;
            growth+=amount;
            progress=growth/growthpoints;
            if (progress>=1){
                getData().putboolean("grown",true);
                progress=1;
             }
        getData().putFloat("growth",growth);
        getData().putFloat("progress",progress);
        return progress;

    }

}
