package com.hgs.Model.Inventory.Item.Crop;

import com.hgs.Model.Inventory.Item.Item;
import com.hgs.Model.Inventory.Item.Items;
import com.hgs.Model.Inventory.Item.Seed.CarrotSeed;

public class Potato extends  Crop{
    public Potato() {
        super("Potato",5, 0.25f, 0.25f);
    }
}
