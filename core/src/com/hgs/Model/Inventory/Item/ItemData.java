package com.hgs.Model.Inventory.Item;


import java.util.HashMap;

public class ItemData {
    private final HashMap<String,String> data=new HashMap<>();

    public void putString(String key,String val){
        data.put(key,val);
    }

    public String getString(String key,String defval){
        if (data.containsKey(key))return data.get(key);
        return defval;
    }

    public void putLong(String key,long val){
        data.put(key,String.valueOf(val));
    }

    public long getLong(String key,long defval){
        if (data.containsKey(key))return Long.parseLong(data.get(key));
        return defval;
    }

    public void putFloat(String key,float val){
        data.put(key,String.valueOf(val));
    }

    public float getFloat(String key,float defval){
        if (data.containsKey(key))return Float.parseFloat(data.get(key));
        return defval;
    }

    public void putDouble(String key,double val){
        long longval= Double.doubleToRawLongBits(val);
        putLong(key,longval);
    }

    public double getDouble(String key,double defval){
        if (data.containsKey(key)) return Double.longBitsToDouble(getLong(key,0));
        return defval;
    }

    public void putboolean(String key,boolean val){
        data.put(key,String.valueOf(val));
    }

    public boolean getboolean(String key,boolean defval){
        if (data.containsKey(key)) return Boolean.parseBoolean(data.get(key));
        return defval;
    }

    public void copydata(HashMap<String,String>data){
        data.clear();
        data.putAll(data);
    }

    public HashMap<String,String> getdata(){
        return data;
    }


}
