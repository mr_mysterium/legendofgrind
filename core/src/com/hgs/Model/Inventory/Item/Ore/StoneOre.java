package com.hgs.Model.Inventory.Item.Ore;

import com.hgs.Model.Inventory.Item.Resource.Stone;

public class StoneOre extends Ore {
    public StoneOre() {
        super("StoneOre",25,new Stone(),2.5f,2.5f);
    }
}
