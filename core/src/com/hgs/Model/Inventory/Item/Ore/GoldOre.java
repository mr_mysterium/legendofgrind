package com.hgs.Model.Inventory.Item.Ore;

import com.hgs.Model.Inventory.Item.Resource.GoldChunk;

public class GoldOre extends Ore {
    public GoldOre() {
        super("GoldOre",100,new GoldChunk(),15,15);
    }
}
