package com.hgs.Model.Inventory.Item.Tool;

import com.hgs.Model.Inventory.Item.Item;

public abstract class Tool extends Item {

    public Tool(String name,float maxdurability) {
        super(name,1);
        getData().putFloat("maxdurability",maxdurability);
        getData().putFloat("durability",maxdurability);
    }
}
