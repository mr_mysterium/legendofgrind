package com.hgs.Model.Inventory.Item.Seed;

import com.hgs.Model.Inventory.Item.Crop.Crop;
import com.hgs.Model.Inventory.Item.Crop.Potato;

public class PotatoSeed extends Seed {
    public PotatoSeed() {
        super("PotatoSeed",new Potato());
    }
}
