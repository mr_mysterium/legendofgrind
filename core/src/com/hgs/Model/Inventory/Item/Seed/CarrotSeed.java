package com.hgs.Model.Inventory.Item.Seed;

import com.hgs.Model.Inventory.Item.Crop.Carrot;
import com.hgs.Model.Inventory.Item.Crop.Crop;

public class CarrotSeed extends Seed{
    public CarrotSeed() {
        super("CarrotSeed",new Carrot());
    }
}
