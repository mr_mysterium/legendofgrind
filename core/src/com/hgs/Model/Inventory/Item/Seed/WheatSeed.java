package com.hgs.Model.Inventory.Item.Seed;

import com.hgs.Model.Inventory.Item.Crop.Crop;
import com.hgs.Model.Inventory.Item.Crop.Wheat;

public class WheatSeed extends Seed {
    public WheatSeed() {
        super("WheatSeed", new Wheat());
    }
}
