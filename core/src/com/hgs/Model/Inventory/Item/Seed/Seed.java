package com.hgs.Model.Inventory.Item.Seed;

import com.hgs.Model.Inventory.Item.Crop.Crop;
import com.hgs.Model.Inventory.Item.Item;

public abstract class Seed extends Item {
    public Seed(String name,Crop crop) {
        super(name);
        getData().putString("category","Seed");
        getData().putString("crop",crop.getName());
    }
}
