package com.hgs.Model.Inventory.Crafting;

import com.hgs.Model.Inventory.Item.ItemStack;
import com.hgs.Model.Inventory.Item.Items;

public enum Recipes {
    Hoe(new ItemStack[]{new ItemStack(Items.Wood.getItem(),3)}, new ItemStack(Items.Hoe.getItem(),1), 10,10,10),
    Pickaxe(new ItemStack[]{new ItemStack(Items.Wood.getItem(),3)}, new ItemStack(Items.Pickaxe.getItem(),1), 10, 10, 10);

    private final ItemStack[] material;
    private final ItemStack item;
    private final float craftpoints;
    private final float money;
    private final float xp;

    Recipes(ItemStack[] material, ItemStack item, float craftpoints, float money, float xp) {
        this.material = material;
        this.item = item;
        this.craftpoints = craftpoints;
        this.money = money;
        this.xp = xp;
    }

    public ItemStack[] getMaterial() {
        return material;
    }

    public ItemStack getItem() {
        return item;
    }

    public float getCraftpoints() {
        return craftpoints;
    }

    public float getMoney() {
        return money;
    }

    public float getXp() {
        return xp;
    }
}
