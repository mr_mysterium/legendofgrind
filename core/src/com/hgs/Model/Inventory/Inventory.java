package com.hgs.Model.Inventory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;

public class Inventory {

    final Vector2 dimensions;
    final int slotsize;
    final int slotcount;
    final Array<InventorySlot> slots;
    final Table table;

    public Inventory(Skin skin, Vector2 dimensions, int slotsize) {
        table = new Table();
        slots = new Array<>();
        this.slotsize = slotsize;
        this.dimensions = dimensions;
        this.slotcount = (int) (dimensions.x * dimensions.y);
        int index=0;
        for (int i = 0; i < dimensions.y; i++) {
            for (int j = 0; j < dimensions.x; j++) {
                InventorySlot slot = new InventorySlot(index,slotsize);
                index++;
                table.add(slot).fill();
                slots.add(slot);
            }
            table.row();
        }

    }


    public Table getTable() {
        return table;
    }

    public Array<InventorySlot> getSlots() {
        return slots;
    }
}
