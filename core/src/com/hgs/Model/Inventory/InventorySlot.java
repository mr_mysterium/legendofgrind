package com.hgs.Model.Inventory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.Inventory.Item.Item;
import com.hgs.Model.Inventory.Item.ItemData;
import com.hgs.Model.Inventory.Item.Tool.Tool;
import com.hgs.Model.LOG;
import com.hgs.Model.Util.HSL;

public class InventorySlot extends Group {
    final int index;
    final int slotsize;
    final Image background;
    Image itemimage;
    final Label info;
    final Container<Image> itemcontainer;
    final Container<Label>infocontainer;
    ProgressBar.ProgressBarStyle prgstyle;
    final Texture durabilitytex;
    final ProgressBar durabilitybar;


    public InventorySlot(int index,int slotsize){
        itemcontainer=new Container<Image>();
        infocontainer=new Container<Label>();
        itemimage= new Image();
        info = new Label("", LOG.skin,"font", Color.WHITE);
        this.index= index;
        this.slotsize=slotsize;
        setSize(slotsize,slotsize);
        background= new Image(new NinePatchDrawable(LOG.skin.getPatch("progressbg")));
        addActor(background);
        infocontainer.setActor(info);
        itemcontainer.setActor(itemimage);
        itemcontainer.fill(0.7f,0.7f);
        info.setAlignment(Align.center);
        infocontainer.setSize(background.getWidth(),background.getHeight()/10);
        ProgressBar.ProgressBarStyle prgstyle = new ProgressBar.ProgressBarStyle();
        durabilitytex= new Texture(Gdx.files.internal("progress.png"));
        prgstyle.knobBefore = new NinePatchDrawable(LOG.skin.getPatch("progress"));
        prgstyle.background = new NinePatchDrawable(LOG.skin.getPatch("progressbg"));
        durabilitybar = new ProgressBar(0,1,0.01f,false,prgstyle);
        durabilitybar.setVisible(false);
        addActor(itemcontainer);
        addActor(infocontainer);
        addActor(durabilitybar);

    }

    public int getSlotsize() {
        return slotsize;
    }

    public void setSlotItem(Item item, int count){
        itemimage= new Image(item.getImage());
        itemcontainer.setActor(itemimage);
        if (count==1){
            info.setText(item.getName());
        }
        else
        info.setText(item.getName()+" x"+count);
        ItemData data= item.getData();
        float durability=item.getData().getFloat("durability",-1);
        float maxdurability=item.getData().getFloat("maxdurability",-1);
        if (maxdurability!=-1)
        {
            float durabilityleft= durability/maxdurability;
            if (durabilityleft==1)durabilitybar.setVisible(false);
        else durabilitybar.setVisible(true);
        {
            durabilitytex.getTextureData().prepare();
            Pixmap progresspixmap= durabilitytex.getTextureData().consumePixmap();
            for (int y = 0; y < progresspixmap.getHeight(); y++) {
                for (int x = 0; x < progresspixmap.getWidth(); x++) {
                    progresspixmap.setColor(HSL.transition(Color.GREEN,1-durabilityleft));
                    if (progresspixmap.getPixel(x,y)==-1)progresspixmap.fillRectangle(x,y,1,1);
                }
            }
            durabilitybar.getStyle().knobBefore= new TextureRegionDrawable(new Texture(progresspixmap));
            durabilitybar.getStyle().knobBefore.setMinWidth(1);
            durabilitybar.getStyle().knobBefore.setMinHeight(durabilitybar.getHeight());
            durabilitybar.setValue(durabilityleft);
          }
        }
    }

    public void ClearSlot(){
        itemimage=new Image();
        durabilitybar.setVisible(false);
        itemcontainer.setActor(itemimage);
        info.setText("");
    }

    public void scale(float ratio){
        background.setSize((int)Math.ceil(slotsize*ratio),(int)Math.ceil(slotsize*ratio));
        itemcontainer.setSize(slotsize*ratio,slotsize*ratio);
        infocontainer.setSize(slotsize*ratio,slotsize*ratio/5);
        infocontainer.setPosition(0,background.getHeight()-infocontainer.getHeight());
        ProgressBar.ProgressBarStyle dbstyle= durabilitybar.getStyle();
        durabilitybar.setSize(background.getWidth()*0.7f,background.getHeight()/8);
        durabilitybar.setPosition(background.getWidth()/2- durabilitybar.getWidth()/2,background.getHeight()/8);
        dbstyle.background.setMinWidth(durabilitybar.getWidth());
        dbstyle.background.setMinHeight(durabilitybar.getHeight());
        dbstyle.knobBefore.setMinWidth(1);
        dbstyle.knobBefore.setMinHeight(durabilitybar.getHeight());
        durabilitybar.setStyle(dbstyle);
        info.setFontScale((float)slotsize/300*ratio);

    }


    public Container<Image> getItemcontainer() {
        return itemcontainer;
    }

    public int getIndex() {
        return index;
    }
}
