package com.hgs.Model.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.hgs.Model.Inventory.Item.Crop.Crop;
import com.hgs.Model.Inventory.Item.ItemData;
import com.hgs.Model.LOG;

public class UIFarm extends UIComponent {
    final LOG log;
    Group farmgroup;
    Container<Image>farmcontainer;
    CooldownTimer timer;
    Crop crop;
    Rectangle croptangle;
    Rectangle farmtangle;
    float offsety;
    float offsetx;
    ParticleEffectPool pool;
    ParticleEmitter emitter;
    ParticleEffect effect;
    FloatingText text;

    public UIFarm(LOG log,UIFarmArray array,Table table, String name, Vector2 size) {
        super(table, name, size);
        this.log=log;
        setStyle();
    }

    public UIFarm(LOG log,UIFarmArray array,Table table, String name, Vector2 size, int[] padding) {
        super(table, name, size, padding);
        this.log=log;
        setStyle();
    }

    @Override
    void setStyle() {
        timer = new CooldownTimer(true);
        timer.setColor(new Color(0/255f,255/255f,185/255f,1f));
        farmcontainer=new Container<Image>();
        farmcontainer.setBackground(LOG.skin.getDrawable("farmbox"));
        farmcontainer.setSize(size.x,size.y);
        farmcontainer.setActor(timer);
        farmgroup = new Group();
        farmgroup.addActorAt(0,farmcontainer);
        farmgroup.addActorAt(1,timer);
        actor= farmgroup;
        croptangle = new Rectangle();
        farmtangle = new Rectangle();
        emitter = new ParticleEmitter();
        effect= new ParticleEffect();
        effect.load(Gdx.files.internal("harvest"), LOG.ItemAtlas);
        pool = new ParticleEffectPool(effect,1,20);
        text = new FloatingText("",1500);

    }

    @Override
    public void scale(float ratio) {
        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        farmcontainer.setSize(table.getCell(actor).getPrefWidth(),table.getCell(actor).getPrefHeight());
        timer.setSize(table.getCell(actor).getPrefWidth()/2,table.getCell(actor).getPrefHeight()/2);
        timer.update(0);
        timer.setPosition(table.getCell(actor).getPrefWidth()*0.2f,table.getCell(actor).getPrefHeight()*0.1f);
        offsetx= table.getCell(actor).getPrefWidth()/6;
        offsety= table.getCell(actor).getPrefHeight()/6;
        text.setDeltaY(farmcontainer.getHeight()/5);
        effect.reset(true);
        effect.scaleEffect(farmcontainer.getWidth()*0.7f);
        pool = new ParticleEffectPool(effect,1,20);
    }

    public void plant(Crop crop){
        this.crop=crop;
    }

    public void update(float delta){
        farmtangle.set(actor.localToStageCoordinates(new Vector2(0, 0)).x ,actor.localToStageCoordinates(new Vector2(0, 0)).y,farmcontainer.getWidth(),farmcontainer.getHeight());
        if (crop!=null) {
        ItemData data= crop.getData();
        float progress = data.getFloat("progress",0);
        timer.update(progress);
        Vector2 containersize = new Vector2(table.getCell(actor).getPrefWidth(), table.getCell(actor).getPrefHeight());
        Vector2 containerorigin = new Vector2(farmcontainer.localToStageCoordinates(new Vector2(0, 0)));
        crop.grow(1 * delta);
        float offsetymax = containersize.y * 0.7f;
        offsety = progress * offsetymax;
        croptangle.set(farmcontainer.localToStageCoordinates(new Vector2(0, 0)).x + offsetx, containerorigin.y + offsety + containersize.y / 6, containersize.x * 0.75f, containersize.y * 0.75f);
        if (!log.farmingscene.getTable().isVisible())return;
        Gdx.gl.glEnable(GL20.GL_SCISSOR_TEST);
        Gdx.gl.glScissor((int) containerorigin.x, (int) (containerorigin.y + containersize.y * 0.85), (int) (containersize.x * 0.75f), (int) (containersize.y * 0.75f));
        LOG.batch.begin();
        LOG.batch.draw(crop.getImage(), croptangle.x, croptangle.y, croptangle.width, croptangle.height);
        LOG.batch.end();
        Gdx.gl.glDisable(GL20.GL_SCISSOR_TEST);
        }
    }

    public Crop getCrop() {
        return crop;
    }

    public Rectangle getCroptangle() {
        return croptangle;
    }

    public void setCroptangle(Rectangle croptangle) {
        this.croptangle = croptangle;
    }

    public Rectangle getFarmtangle() {
        return farmtangle;
    }

    public ParticleEffectPool getPool() {
        return pool;
    }

    public FloatingText getText() {
        return text;
    }
}
