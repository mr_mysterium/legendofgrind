package com.hgs.Model.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.Character.Player;
import com.hgs.Model.Character.Skill;
import com.hgs.Model.Inventory.Item.Crop.Carrot;
import com.hgs.Model.Inventory.Item.Crop.Crop;
import com.hgs.Model.Inventory.Item.Crop.Potato;
import com.hgs.Model.Inventory.Item.Crop.Wheat;
import com.hgs.Model.Inventory.Item.ItemData;
import com.hgs.Model.Inventory.Item.Items;
import com.hgs.Model.LOG;
import com.hgs.Model.Util.Formatting;

import java.lang.reflect.InvocationTargetException;

public class UIFarmArray extends  UIComponent {
    final LOG log;
    Array<UIFarm>farms;
    Table t;
    int amount;
    final Array<ParticleEffectPool.PooledEffect> effects = new Array<ParticleEffectPool.PooledEffect>();
    final Array<FloatingText> floatingtexts = new Array<FloatingText>();
    public UIFarmArray(LOG log,Table table, String name, Vector2 size,int amount) {
        super(table, name, size);
        this.log=log;
        this.amount = amount;
        setStyle();
    }

    public UIFarmArray(LOG log,Table table, String name, Vector2 size, int[] padding) {
        super(table, name, size, padding);
        this.log=log;
        setStyle();
    }

    @Override
    void setStyle() {
        t= new Table();
        farms= new Array<UIFarm>();
        for (int i= 1;i<=amount;i++)
        {
            farms.add(new UIFarm(log,this,t,"farm"+i,new Vector2(size.x/5.5f,size.x/5.5f)));
            t.add(farms.get(i-1).getActor());
            if (i%4==0)t.row();
        }
        actor = t;
    }

    @Override
    public void scale(float ratio) {
        for (UIFarm farm:farms) {
            farm.scale(ratio);
        }
        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);
    }
    public void update(float delta){
        for (UIFarm farm: farms) {
            farm.update(delta);
        }
        LOG.batch.begin();
        for (int i = effects.size - 1; i >= 0; i--) {
            ParticleEffectPool.PooledEffect effect = effects.get(i);
            effect.draw(LOG.batch, delta);
            if (effect.isComplete()) {
                effect.free();
                effects.removeIndex(i);
            }
        }

        for (int i = floatingtexts.size - 1; i >= 0; i--) {
            FloatingText text = floatingtexts.get(i);
            text.draw(LOG.batch,1);
            if (text.isDone())floatingtexts.removeIndex(i);
        }
        LOG.batch.end();
    }
    public boolean harvest(Vector2 position){
        Vector2 touch = new Vector2(position.x,Gdx.graphics.getHeight()-position.y);
        for (UIFarm farm:farms) {

            if (farm.crop!=null&&farm.croptangle.contains(touch)){
                ItemData data= farm.crop.getData();
                boolean grown= data.getboolean("grown",false);
                if (!grown)continue;
                double moneyval= data.getDouble("moneyvalue",-1);
                double xpval= data.getDouble("xpvalue",-1);
                Array<Sprite>sprites= new Array<Sprite>();
                sprites.add(new Sprite(farm.crop.getImage()));
                Player player = log.player;
                Skill farming = player.getFarming();
                player.getInventorymodel().AddItems(farm.crop,1);
                double moneyharvest=moneyval*farming.getmoneymult();
                double xpharvest=xpval*farming.getxpmult();
                farm.crop=null;
                farm.timer.update(0);

                FloatingText text = farm.getText();
                text.setText("[#22ff00]$"+ LOG.formatting.formatdec(moneyharvest) +"\n"+"[#00fffb]XP"+LOG.formatting.formatdec(xpharvest));
                text.setPosition(farm.farmtangle.x+farm.farmtangle.width/2-text.getTextwidth()/2, farm.farmtangle.y+farm.farmtangle.height*1.1f);
                floatingtexts.add(text);
                ParticleEffectPool.PooledEffect effect = farm.pool.obtain();
                effect.setPosition(farm.croptangle.x+farm.croptangle.getWidth()/2,farm.croptangle.y+farm.croptangle.getHeight()/2);
                effect.getEmitters().first().setSprites(sprites);
                effect.start();
                effects.add(effect);
                player.addbalance(moneyharvest);
                farming.addxp(xpharvest);
                text.animate();
                log.player.save();
                return true;
            }
        }
        return false;
    }
    public boolean plant (Vector2 position, Crop crop){
        Vector2 touch = new Vector2(position.x,Gdx.graphics.getHeight()-position.y);
        for (UIFarm farm:farms) {
            if (farm.farmtangle.contains(touch)){
                if (farm.crop==null){
                      farm.plant(crop);

                    log.player.save();
                    return true;
                    }
                }
            }
        log.player.save();
        return false;
    }

    public void save(){
        String farmsave= new String();
        for (int i = 0; i<farms.size; i++){
            UIFarm farm= farms.get(i);
            if (farm.getCrop()==null)continue;
            ItemData data= farm.getCrop().getData();
            float growth=data.getFloat("growth",-1);
            farmsave+=i+","+farm.getCrop().getName()+","+growth+"\n";
        }
        LOG.data.putString("farm",farmsave);
    }

    public void load()  {
        String[] farmdata = LOG.data.getString("farm").split("\n");
        for (int i = 0; i<farmdata.length; i++){
            String[]cropdata= farmdata[i].split(",");
            if (cropdata.length==1)continue;
            int slot = Integer.parseInt(cropdata[0]);
            farms.get(slot).plant((Crop) Items.valueOf(cropdata[1]).getItem());
            farms.get(slot).getCrop().getData().putFloat("growth",Float.parseFloat(cropdata[2]));
        }

    }

    public Array<UIFarm> getFarms() {
        return farms;
    }
}
