package com.hgs.Model.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.LOG;
import com.hgs.Model.Scenes.SceneType;

public class UIProgressTitle extends UIComponent {
    final SceneType type;
    final LOG log;
    final Color color;
    Group progressgroup;
    Button menubutton;
    Label progresslabel;
    ProgressBar pb;
    float fontscale=1;
    Container<Label>labelContainer;
    public UIProgressTitle(LOG log, SceneType type, Table table, String name, Vector2 size, Color color, float fontscale) {
        super(table, name, size);
        this.log=log;
        this.type=type;
        this.color=color;
        this.fontscale=fontscale;
        setStyle();
    }

    public UIProgressTitle(LOG log, SceneType type, Table table, String name, Vector2 size, int[] padding, Color color, float fontscale) {
        super(table, name, size, padding);
        this.log=log;
        this.type=type;
        this.color=color;
        this.fontscale=fontscale;
        setStyle();
    }

    @Override
    void setStyle() {
        progressgroup = new Group();
        progresslabel = new Label("", LOG.skin,"font", Color.WHITE);
        NinePatch knobpatch = new NinePatch(LOG.skin.getPatch("progress"),color);
        NinePatchDrawable knob = new NinePatchDrawable(knobpatch);
        Button.ButtonStyle bs = new Button.ButtonStyle();
        bs.up=LOG.skin.getDrawable("menu");
        bs.down=LOG.skin.getDrawable("menu");
        menubutton = new Button(bs);
        ProgressBar.ProgressBarStyle pbstyle = new ProgressBar.ProgressBarStyle();
        pbstyle.knobBefore= knob;
        pbstyle.knobBefore.setMinWidth(1);
        pbstyle.background= new NinePatchDrawable(LOG.skin.getPatch("progressbg"));
        pb = new ProgressBar(0,1,0.01f,false,pbstyle);
        menubutton.setSize(size.y,size.y);
        pb.setSize(size.x-size.y,size.y);
        progresslabel.setSize(size.x-size.y,size.y);
        progresslabel.setAlignment(Align.center);
        labelContainer= new Container<Label>(progresslabel);
        labelContainer.align(Align.center);
        labelContainer.fill();
        progressgroup.addActor(menubutton);
        progressgroup.addActor(pb);
        progressgroup.addActor(labelContainer);
        actor= progressgroup;

        menubutton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                log.setScene(SceneType.Menu);
                log.menuscene.setLastscene(type);
            }
        });
    }

    @Override
    public void scale(float ratio) {
        table.getCell(actor).size((int) Math.ceil((size.x)*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);
        progresslabel.setFontScale(size.x/200*ratio*fontscale);
        labelContainer.setPosition( (size.y*ratio+table.getCell(actor).getPrefWidth())/2, table.getCell(actor).getPrefHeight()/2);
        ProgressBar.ProgressBarStyle pbstyle = pb.getStyle();
        pb.setPosition(size.y*ratio,0);
        pb.setSize((size.x-size.y)*ratio,size.y*ratio);

        progresslabel.setSize(size.x-size.y,size.y);
        menubutton.setSize(size.y*ratio,size.y*ratio);
        pbstyle.background.setMinWidth((size.x-size.y)*ratio);
        pbstyle.background.setMinHeight(size.y*ratio);
        pbstyle.knobBefore.setMinWidth(1);
        pbstyle.knobBefore.setMinHeight(size.y*ratio);
        pb.setStyle(pbstyle);
        actor= progressgroup;

    }



    public ProgressBar getPb() {
        return pb;
    }

    public float getFontscale() {
        return fontscale;
    }

    public Label getProgresslabel() {
        return progresslabel;
    }

    public void setProgresslabel(Label progresslabel) {
        this.progresslabel = progresslabel;
    }

    public void setFontscale(float fontscale) {
        this.fontscale = fontscale;
    }

    public Button getMenubutton() {
        return menubutton;
    }
}
