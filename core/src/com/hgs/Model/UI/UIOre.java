package com.hgs.Model.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.Character.Player;
import com.hgs.Model.Character.Skill;
import com.hgs.Model.Inventory.Item.Crop.Crop;
import com.hgs.Model.Inventory.Item.Item;
import com.hgs.Model.Inventory.Item.ItemData;
import com.hgs.Model.Inventory.Item.Items;
import com.hgs.Model.Inventory.Item.Ore.Ore;
import com.hgs.Model.LOG;

import java.util.Random;

public class UIOre extends UIComponent {
    LOG log;
    Image oreimage;
    Ore ore;
    double movement;
    double diagonal;
    float threshold=0.5f;
    FloatingText text= new FloatingText("",1500);

    Array<ParticleEffectPool.PooledEffect> effects = new Array();
    ParticleEffectPool pool;
    ParticleEffect effect;
    ParticleEffectPool.PooledEffect pooledeffect;
    final Array<FloatingText> floatingtexts = new Array<FloatingText>();

    public UIOre(LOG log,Table table, String name, Vector2 size) {
        super(table, name, size);
        this.log=log;
        setStyle();
    }

    public UIOre(LOG log,Table table, String name, Vector2 size, int[] padding) {
        super(table, name, size, padding);
        this.log=log;
        setStyle();
    }

    @Override
    void setStyle() {
        ore = generate();
        oreimage = new Image(ore.getImage());
        oreimage.setSize(size.x,size.y);
        actor= oreimage;

        effect= new ParticleEffect();
        effect.load(Gdx.files.internal("stoneparticle"), LOG.ParticleAtlas);
        pool = new ParticleEffectPool(effect,20,20);

    }

    private Ore generate() {
        Random r = new Random();
        float c = r.nextFloat();
        if (c>0.975)return (Ore) Items.DiamondOre.getItem();
        if (c>0.825)return (Ore) Items.GoldOre.getItem();
        if (c>0.725)return (Ore) Items.IronOre.getItem();
        return (Ore) Items.StoneOre.getItem();
    }

    public boolean mine(double dist,float damage){
        ItemData oredata= ore.getData();
        Player player = log.player;
        Skill mining = player.getMining();
        double distance = dist;
        movement+=distance;

        if (movement> diagonal*threshold){
            float hp=ore.getData().getFloat("hp",-1)-damage;
            oredata.putFloat("hp",hp);
            pooledeffect = pool.obtain();
            pooledeffect.setPosition(Gdx.input.getX(),Gdx.graphics.getHeight()-Gdx.input.getY());
            pooledeffect.start();
            effects.add(pooledeffect);
            movement=0;

            if (hp<=0){

                Item drop= Items.valueOf(ore.getData().getString("drop","undefined")).getItem();
                double moneyval= oredata.getDouble("moneyvalue",-1);
                double xpval= oredata.getDouble("xpvalue",-1);
                double moneyharvest= moneyval*mining.getmoneymult();
                double xpharvest=xpval*mining.getxpmult();

                player.addbalance(moneyharvest);
                mining.addxp(xpharvest);

                text= new FloatingText("",1500);
                text.setDeltaY(oreimage.getHeight()/20);
                text.setText("[#22ff00]$"+ LOG.formatting.formatdec(moneyharvest) +"\n"+"[#00fffb]XP"+LOG.formatting.formatdec(xpharvest));
                text.setPosition(oreimage.getX()+oreimage.getWidth()/2-text.getTextwidth()/2, oreimage.getY()+oreimage.getHeight()*0.9f);
                floatingtexts.add(text);
                text.animate();

                log.player.getInventorymodel().AddItems(drop,1);
                ore=generate();
                oreimage.setDrawable(new TextureRegionDrawable(ore.getImage()));
            }
            log.player.save();
            return true;
        }
        log.player.save();
        return false;
    }

    public void update(float delta){
        LOG.batch.begin();
        for (int i = effects.size - 1; i >= 0; i--) {
            ParticleEffectPool.PooledEffect effect = effects.get(i);
            effect.draw(LOG.batch, delta);
            if (effect.isComplete()) {
                effect.free();
                effects.removeIndex(i);
            }
        }
        for (int i = floatingtexts.size - 1; i >= 0; i--) {
            FloatingText text = floatingtexts.get(i);
            text.draw(LOG.batch,1);
            if (text.isDone())floatingtexts.removeIndex(i);
        }
        LOG.batch.end();
    }

    @Override
    public void scale(float ratio) {
        diagonal = Math.sqrt(Math.pow(Gdx.graphics.getWidth(),2)+Math.pow(Gdx.graphics.getHeight(),2));
        effect.reset(true);
        effect.scaleEffect(size.x*ratio*0.1f);
        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);
    }

    public void save(){
            String minesave= new String();
            ItemData data= ore.getData();
            float growth=data.getFloat("growth",-1);
            minesave+=ore.getName()+","+ore.getData().getFloat("hp",-1);
            LOG.data.putString("mine",minesave);
    }

    public void load()  {
            String[]minedata= LOG.data.getString("mine").split(",");
            if (minedata.length==1){
                return;
            }
            ore= (Ore) Items.valueOf(minedata[0]).getItem();
            oreimage.setDrawable(new TextureRegionDrawable(ore.getImage()));
            ore.getData().putFloat("hp",Float.parseFloat(minedata[1]));
    }

    public Image getOreimage() {
        return oreimage;
    }

    public Rectangle getbounds(){
        Rectangle r= new Rectangle(oreimage.getX(),oreimage.getY(),oreimage.getWidth(),oreimage.getHeight());
        return r;
    }

    public Ore getOre() {
        return ore;
    }
}
