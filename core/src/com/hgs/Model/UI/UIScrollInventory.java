package com.hgs.Model.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.hgs.Model.Inventory.Inventory;
import com.hgs.Model.Inventory.InventorySlot;
import com.hgs.Model.LOG;

public class UIScrollInventory extends UIComponent{
    Vector2 dimensions;
    final int slotsize;
    Inventory iv;
    Container<ScrollPane> container;
    ScrollPane pane;




    public UIScrollInventory(Table table, String name, Vector2 size, Vector2 dimensions, int slotsize) {
        super(table, name, size);
        this.dimensions= new Vector2();
        this.dimensions=dimensions;
        this.slotsize=slotsize;
        setStyle();
    }

    public UIScrollInventory(Table table, String name, Vector2 size, Vector2 dimensions, int slotsize, int[] padding) {
        super(table, name, size, padding);
        this.dimensions= new Vector2();
        this.dimensions=dimensions;
        this.slotsize=slotsize;
        setStyle();
    }

    @Override
    void setStyle() {
        iv= new Inventory(LOG.skin,dimensions,slotsize);
        pane=new ScrollPane(iv.getTable());
        pane.setOverscroll(false,false);
        pane.setScrollingDisabled(true,false);
        container=new Container<ScrollPane>();
        container.setActor(pane);
        container.fill(0.99f,0.99f);
        container.setBackground(new NinePatchDrawable(LOG.skin.getPatch("progressbg")));
        actor=container;
    }

    @Override
    public void scale(float ratio) {
        for (InventorySlot s: iv.getSlots()) {
            s.scale(ratio);
//            iv.getTable().setSize((int)Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
            iv.getTable().getCell(s).size((int) Math.ceil(s.getSlotsize()*ratio),(int)Math.ceil(s.getSlotsize()*ratio));
        }
        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
    }

    public Vector2 getDimensions() {
        return dimensions;
    }

    public Container<ScrollPane> getContainer() {
        return container;
    }

    public ScrollPane getPane() {
        return pane;
    }

    public int getSlotsize() {
        return slotsize;
    }

    public Inventory getinventory() {
        return iv;
    }
}
