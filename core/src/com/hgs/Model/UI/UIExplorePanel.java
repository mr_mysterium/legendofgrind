package com.hgs.Model.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.Character.Player;
import com.hgs.Model.Character.Skill;
import com.hgs.Model.Inventory.InventorySlot;
import com.hgs.Model.Inventory.Item.Item;
import com.hgs.Model.Inventory.Item.ItemData;
import com.hgs.Model.Inventory.Item.Items;
import com.hgs.Model.LOG;
import com.hgs.Model.Util.Formatting;
import com.hgs.Model.World.Zone.Forest;
import com.hgs.Model.World.Zone.Hills;
import com.hgs.Model.World.Zone.Zone;
import com.hgs.Model.World.Zone.Zones;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class UIExplorePanel extends UIComponent {

    Zone zone;
    boolean exploring;
    boolean collectable;
    float stepstarget;
    float steps=0;
    float stepprogress=0;

    LOG log;
    Container<Table> container;
    Formatting formatting;
    Table t;
    UILabel zonelabel;
    UISelectBox box;
    UILabel lootpvlabel;
    Item[]items;
    HashMap<String,Integer>amounts= new HashMap<String,Integer>();
    HashMap<String,InventorySlot> lootpreview = new HashMap<String, InventorySlot>();
    HashMap<String,Container>amountlabels= new HashMap<String,Container>();
    HorizontalGroup lootpvgroup;
    HorizontalGroup lootamountgroup;
    UILabel rewardlabel;
    UIButton startbutton;
    UIProgressbar exploreprogress;
    UIButton collectbtn;
    Array<UIComponent> components;
    Label.LabelStyle style= new Label.LabelStyle();

    public UIExplorePanel(LOG log,Table table, String name, Vector2 size) {
        super(table, name, size);
        this.log=log;
        setStyle();
    }

    public UIExplorePanel(LOG log,Table table, String name, Vector2 size, int[] padding) {
        super(table, name, size, padding);
        this.log=log;
        setStyle();
    }

    @Override
    void setStyle() {
        formatting= new Formatting();
        t=new Table();
        t.top();
        zonelabel = new UILabel("Zone:", Align.center,t,"zonelabel",new Vector2(size.x/2.5f,size.x/8f),1.5f,new int[]{0,10,0,0});
        box= new UISelectBox(t,"selectbox",new Vector2(size.x/2.5f,size.x/8f));
        box.getBox().setItems(Zones.getzones());
        zone=Zones.valueOf(box.getBox().getSelected()).getZone();
        lootpvlabel = new UILabel("Possible Loot:", Align.center,t,"lootpblabel",new Vector2(size.x,size.x/8f),0.7f);
        lootpvgroup= new HorizontalGroup();
        lootamountgroup = new HorizontalGroup();
        rewardlabel = new UILabel("", Align.center,t,"zonelabel",new Vector2(size.x,size.x/8f),0.5f,new int[]{0,10,0,0});
        rewardlabel.getActor().setVisible(false);
        startbutton= new UIButton("",t, new NinePatchDrawable(LOG.skin.getPatch("progressbg")),"startbtn",new Vector2(size.x*0.9f,size.x/8f),0.7f);
        exploreprogress= new UIProgressbar(t,"explorepb",new Vector2(size.x*0.9f,size.x/8f),new Color(0,1,0,1),0.5f);
        collectbtn= new UIButton("Collect",t, new NinePatchDrawable(LOG.skin.getPatch("progressbg")),"collectbtn",new Vector2(size.x*0.9f,size.x/8f),0.7f);
        style.font= LOG.font;
        load();
        t.add(zonelabel.getActor());
        t.add(box.getActor());
        t.row();
        t.add().expandY().colspan(2);
        t.row();
        t.add(lootpvlabel.getActor()).colspan(2);
        t.row();
        for (int i=0;i<items.length;i++){
            InventorySlot slot = new InventorySlot(i,(int)size.y/8);
            Label label = new Label("",style);
            label.setAlignment(Align.center);
            Container container = new Container<Label>(label).width(size.y/8);
            slot.setSlotItem(items[i],1);
            lootpreview.put(items[i].getName(),slot);
            amountlabels.put(items[i].getName(),container);
            lootpvgroup.addActor(slot);
            lootamountgroup.addActor(container);
        }
        t.add(lootpvgroup).colspan(2);
        t.row();
        t.add(lootamountgroup).colspan(2);
        t.row();
        t.add().expandY().colspan(2);
        t.row();
        t.add(rewardlabel.getActor()).colspan(2);
        t.row();
        t.add(startbutton.getButton()).colspan(2);
        container= new Container<Table>(t);
        container.fill(1,0.95f);
        container.top();
        components= new Array<UIComponent>();
        components.add(box,zonelabel,lootpvlabel, rewardlabel);
        components.add(startbutton);
        actor=container;
        if (exploring)explore();



        box.getBox().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
               changezone(Zones.valueOf(box.getBox().getSelected()).getZone());
            }
        });

        startbutton.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (!exploring) {
                    exploring = true;
                    t.addAction(sequence(fadeOut(0.0f, Interpolation.fade), run(new Runnable() {
                        @Override
                        public void run() {
                            showExploration();
                        }
                    }), fadeIn(0.0f, Interpolation.fade)));
                }
            }
        });

        collectbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (collectable) {
                    collectable=false;
                    t.addAction(sequence(fadeOut(0.0f, Interpolation.fade), run(new Runnable() {
                        @Override
                        public void run() {
                            collect();
                        }
                    }), fadeIn(0.0f, Interpolation.fade)));
                }
            }
        });
    }

    private void loadzone(Zone selected) {
        this.zone=selected;
        box.getBox().setSelected(selected.getName());
        stepstarget=zone.getSteps();
        items= zone.getLoot().getItems();
        for (int i=0;i<items.length;i++)amounts.put(items[i].getName(),0);
        startbutton.getButton().setText("Start "+"("+formatting.formatdec(stepstarget)+" steps)");
    }

    private void changezone(Zone selected){
        this.zone=selected;
        stepstarget=zone.getSteps();
        items= zone.getLoot().getItems();
        startbutton.getButton().setText("Start "+"("+formatting.formatdec(stepstarget)+" steps)");

        t.getCell(lootpvgroup).height(lootpvgroup.getPrefHeight());
        t.getCell(lootamountgroup).height(lootamountgroup.getPrefHeight());



        for(Map.Entry<String,InventorySlot> entry : lootpreview.entrySet()) {
            String key = entry.getKey();
            InventorySlot value = entry.getValue();
            value.remove();
        }
        for(Map.Entry<String,Container> entry : amountlabels.entrySet()) {
            String key = entry.getKey();
            Container value = entry.getValue();
            value.remove();
        }

        lootpreview.clear();
        amountlabels.clear();


        for(Map.Entry<String,InventorySlot> entry : lootpreview.entrySet()) {
            String key = entry.getKey();
            InventorySlot value = entry.getValue();
            lootpvgroup.addActor(value);
        }

        for(Map.Entry<String,Container> entry : amountlabels.entrySet()) {
            String key = entry.getKey();
            Container value = entry.getValue();
            Label label=(Label) value.getActor();
            label.setText("");
            lootamountgroup.addActor(value);
        }

        for (int i=0;i<items.length;i++){
            InventorySlot slot = new InventorySlot(i,(int)size.y/8);
            Label label = new Label("",style);
            label.setAlignment(Align.center);
            Container container = new Container<Label>(label).width(size.y/8);
            slot.setSlotItem(items[i],1);
            lootpreview.put(items[i].getName(),slot);
            amountlabels.put(items[i].getName(),container);
            amounts.put(items[i].getName(),0);
            lootpvgroup.addActor(slot);
            lootamountgroup.addActor(container);
        }


        scale(LOG.gs.getratio());
    }


    @Override
    public void scale(float ratio) {


        lootpvgroup.space(size.x/20*ratio);
        lootamountgroup.space(size.x/20*ratio);

        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        table.getCell(actor).padTop(padding[0]*ratio);
        table.getCell(actor).padRight(padding[1]*ratio);
        table.getCell(actor).padBottom(padding[2]*ratio);
        table.getCell(actor).padLeft(padding[3]*ratio);
        for (UIComponent component:components)component.scale(ratio);


        for(Map.Entry<String,InventorySlot> entry : lootpreview.entrySet()) {
            String key = entry.getKey();
            InventorySlot value = entry.getValue();
            value.setSize(value.getSlotsize()*ratio,value.getSlotsize()*ratio);
            value.scale(ratio);
        }

        for(Map.Entry<String,Container> entry : amountlabels.entrySet()) {
            String key = entry.getKey();
            Container value = entry.getValue();
            value.width(size.y/8*ratio);
            Label label = (Label)value.getActor();
            label.setFontScale(value.getPrefWidth()/50*ratio);
        }

    }

    public void showSelection(){
        box.getBox().setDisabled(false);
        box.getBox().setTouchable(Touchable.enabled);
        Cell cell;
        cell= t.getCell(collectbtn.getButton());
        t.removeActor(collectbtn.getButton());
        t.getCells().removeValue(cell,true);
        components.removeValue(collectbtn,false);

        for(Map.Entry<String,InventorySlot> entry : lootpreview.entrySet()) {
            String key = entry.getKey();
            InventorySlot value = entry.getValue();
            value.remove();
        }

        for(Map.Entry<String,InventorySlot> entry : lootpreview.entrySet()) {
            String key = entry.getKey();
            InventorySlot value = entry.getValue();
            lootpvgroup.addActor(value);
        }


        for(Map.Entry<String,Container> entry : amountlabels.entrySet()) {
            String key = entry.getKey();
            Container value = entry.getValue();
            value.remove();
        }

        for(Map.Entry<String,Container> entry : amountlabels.entrySet()) {
            String key = entry.getKey();
            Container value = entry.getValue();
            Label label=(Label) value.getActor();
            label.setText("");
            lootamountgroup.addActor(value);
        }


        t.invalidate();
        t.row();
        t.add(startbutton.getActor()).colspan(2);

        lootpvlabel.getLabel().setText("Possible Loot:");
        components.add(startbutton);
        rewardlabel.getActor().addAction(fadeOut(0.0f,Interpolation.fade));
        scale(LOG.gs.getratio());

    }
    public void showExploration(){
        box.getBox().setDisabled(true);
        box.getBox().setTouchable(Touchable.disabled);
        Cell cell;
        cell= t.getCell(startbutton.getButton());
        t.removeActor(startbutton.getButton());
        t.getCells().removeValue(cell,true);
        components.removeValue(startbutton,false);

        t.getCell(lootpvgroup).height(lootpvgroup.getPrefHeight());
        t.getCell(lootamountgroup).height(lootamountgroup.getPrefHeight());
        for(Map.Entry<String,InventorySlot> entry : lootpreview.entrySet()) {
            String key = entry.getKey();
            InventorySlot value = entry.getValue();
            value.remove();
        }
        for(Map.Entry<String,Container> entry : amountlabels.entrySet()) {
            String key = entry.getKey();
            Container value = entry.getValue();
            value.remove();
        }
        t.invalidate();
        t.row();
        t.add(exploreprogress.getActor()).colspan(2);
        components.add(exploreprogress);
        lootpvlabel.getLabel().setText("Found Loot:");

        Player player=log.player;
        Skill exploration= player.getExploration();
        double moneyval= zone.getMoneyreward();
        double xpval= zone.getXpreward();
        double moneyharvest= moneyval*exploration.getmoneymult();
        double xpharvest=xpval*exploration.getxpmult();
        for(Map.Entry<String,InventorySlot> entry : lootpreview.entrySet()) {
            String key = entry.getKey();
            InventorySlot value = entry.getValue();
            if (amounts.get(key).intValue()<=0) continue;
            if (!value.isDescendantOf(lootpvgroup)) {
                lootpvgroup.addActor(value);
                value.addAction(fadeOut(0f, Interpolation.fade));
                value.addAction(fadeIn(0f, Interpolation.fade));
                lootamountgroup.addActor(amountlabels.get(key));
                amountlabels.get(key).addAction(fadeOut(0f, Interpolation.fade));
                amountlabels.get(key).addAction(fadeIn(0f, Interpolation.fade));

            }
        }
        for(Map.Entry<String,Container> entry : amountlabels.entrySet()) {
            String key = entry.getKey();
            Container value = entry.getValue();
            if (amounts.get(key).intValue()>0) {

                Label lbl = (Label) value.getActor();
                lbl.setText(amounts.get(key).intValue() + "x");
            }

            }
        save();
        scale(LOG.gs.getratio());
    }

    public void update(float delta){
        if (exploring) {
            stepprogress += delta;
            while (stepprogress > 1) step();
            exploreprogress.getPb().setValue((steps+stepprogress) / stepstarget);
            exploreprogress.getProgresslabel().setText(formatting.formatdec(stepstarget-steps)+" steps left" +" - "+formatting.formatdec(steps/stepstarget*100)+"%");

        }
    }

    public void explore(){
            t.addAction(sequence(fadeOut(0f, Interpolation.fade), run(new Runnable() {
                @Override
                public void run() {
                    showExploration();
                }
            }), fadeIn(0f, Interpolation.fade)));

    }

    public void step(){
        save();
        stepprogress--;
        steps++;
        Random r = new Random();
        if (steps<=stepstarget-1&&r.nextFloat()>0.5)addloot();
        if (steps>=stepstarget)complete();
    }



    public void addloot(){
        Item item= zone.getLoot().loot();
        if (item==null)return;
        amounts.put(item.getName(),amounts.get(item.getName()).intValue()+1);
        Label label=(Label) amountlabels.get(item.getName()).getActor();
        label.setText(amounts.get(item.getName())+"x");
        for(Map.Entry<String,InventorySlot> entry : lootpreview.entrySet()) {
            String key = entry.getKey();
            InventorySlot value = entry.getValue();
            if (key.equals(item.getName()) && !value.isDescendantOf(lootpvgroup)){
                lootpvgroup.addActor(value);
                value.addAction(fadeOut(0f,Interpolation.fade));
                value.addAction(fadeIn(0f,Interpolation.fade));
                lootamountgroup.addActor(amountlabels.get(key));
                amountlabels.get(key).addAction(fadeOut(0f,Interpolation.fade));
                amountlabels.get(key).addAction(fadeIn(0f,Interpolation.fade));

            }
        }
    }

    public void complete(){
        Player player=log.player;
        Skill exploration= player.getExploration();
        double moneyval= zone.getMoneyreward();
        double xpval= zone.getXpreward();
        String moneyharvest= formatting.formatdec(moneyval*exploration.getmoneymult());
        String xpharvest= formatting.formatdec(xpval*exploration.getxpmult());

        steps=0;
        stepprogress=0;
        save();
        Cell cell;
        cell= t.getCell(exploreprogress.getActor());
        t.removeActor(exploreprogress.getActor());
        t.getCells().removeValue(cell,true);
        components.removeValue(exploreprogress,false);
        t.invalidate();
        t.row();
        t.add(collectbtn.getActor()).colspan(2);
        components.add(collectbtn);
        rewardlabel.getLabel().setText("Reward: "+"[#00FF00]$"+moneyharvest+ " [#00FFFF]"+xpharvest+"XP");
        rewardlabel.getActor().setVisible(true);
        rewardlabel.getActor().addAction(fadeOut(0f,Interpolation.fade));
        rewardlabel.getActor().addAction(sequence(fadeIn(0f,Interpolation.fade),run(new Runnable() {
            @Override
            public void run() {
                collectable=true;
                exploring=false;
            }
        })));
        scale(LOG.gs.getratio());
    }

    public void collect() {
        Player player=log.player;
        Skill exploration= player.getExploration();
        double moneyval= zone.getMoneyreward();
        double xpval= zone.getXpreward();
        double moneyharvest= moneyval*exploration.getmoneymult();
        double xpharvest=xpval*exploration.getxpmult();

        player.addbalance(moneyharvest);
        exploration.addxp(xpharvest);

        for(Map.Entry<String,Integer> entry : amounts.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            if(value>0)player.getInventorymodel().AddItems(Items.valueOf(key).getItem(),value);
            amounts.put(key,0);
        }


        showSelection();
    }

    public void load(){
        String[]minedata= LOG.data.getString("explore","undefined").split("\n");
        if (minedata[0].equals("undefined")){
            loadzone(zone);
            return;
        }
        if (minedata.length==1){
            loadzone(Zones.valueOf(minedata[0]).getZone());
            return;
        }

        if (minedata[0].equals("collect")){
            collectable=true;
            loadzone(Zones.valueOf(minedata[1]).getZone());
            steps= Zones.valueOf(minedata[1]).getZone().getSteps();
            exploring=true;
            for (int i=2;i<minedata.length;i++){
                String[]lootdata = minedata[i].split(",");
                if (lootdata.length>1)amounts.put(lootdata[0],Integer.parseInt(lootdata[1]));
            }
            return;
        }



        loadzone(Zones.valueOf(minedata[0]).getZone());
        steps= Float.parseFloat(minedata[1]);
        exploring=true;
        for (int i=2;i<minedata.length;i++){
            String[]lootdata = minedata[i].split(",");
            if (lootdata.length>1) amounts.put(lootdata[0],Integer.parseInt(lootdata[1]));
        }
    }

    public void save(){
        if (!exploring){
            if (collectable){

                String exploresave= new String();
                exploresave+= "collect\n";
                exploresave+= zone.getName()+"\n";
                for(Map.Entry<String,Integer> entry : amounts.entrySet()) {
                    String key = entry.getKey();
                    Integer value = entry.getValue();
                    exploresave+=key+","+value+"\n";
                }
                LOG.data.putString("explore",exploresave);
                LOG.data.flush();
                return;
            }
            LOG.data.putString("explore",zone.getName());
            LOG.data.flush();
            return;
        }
        String exploresave= new String();
        String zonesave=zone.getName();
        exploresave+=zonesave+"\n";
        exploresave+=steps+"\n";
        for(Map.Entry<String,Integer> entry : amounts.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();
            exploresave+=key+","+value+"\n";
        }
        exploresave.trim();
        LOG.data.putString("explore",exploresave);
        LOG.data.flush();
    }

    public void render(Batch batch){
        box.render(batch);
    }


}
