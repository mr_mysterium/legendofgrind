package com.hgs.Model.UI;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;


public abstract class UIComponent {
    static int id;
    final Table table;
    final String name;
    Actor actor;
    final Vector2 size;
    final int[] padding;

    public UIComponent(Table table, String name, Vector2 size){
        this.table = table;
        this.name=name;
        this.size= size;
        this.padding = new int[] {0,0,0,0};
        id++;
    }

    public UIComponent(Table table, String name, Vector2 size, int[]padding){
        this.table = table;
        this.name=name;
        this.size= size;
        this.padding= padding;
        id++;
    }

    abstract void setStyle();

    public abstract void scale(float ratio);
    public Actor getActor() {
        return actor;
    }
    public Vector2 getSize() {
        return size;
    }
    public int[] getPadding() {
        return padding;
    }
}
