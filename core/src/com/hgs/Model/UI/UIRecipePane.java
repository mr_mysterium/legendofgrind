package com.hgs.Model.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.Inventory.Crafting.Process;
import com.hgs.Model.Inventory.Item.Item;
import com.hgs.Model.Inventory.Crafting.Recipes;
import com.hgs.Model.LOG;
import com.hgs.Model.Scenes.CraftScene;

public class UIRecipePane extends UIComponent {
    CraftScene craftscene;
    Table t;
    ScrollPane pane;
    Container<ScrollPane> container;
    Array<Container> containers;
    BitmapFont font;

    public UIRecipePane(CraftScene craftscene,Table table, String name, Vector2 size) {
        super(table, name, size);
        this.craftscene=craftscene;
        setStyle();
    }

    public UIRecipePane(CraftScene craftscene,Table table, String name, Vector2 size, int[] padding) {
        super(table, name, size, padding);
        this.craftscene=craftscene;
        setStyle();
    }

    @Override
    void setStyle() {
        font= new BitmapFont(Gdx.files.internal("font.fnt"));
        containers = new Array<Container>();
        t= new Table();
        t.top();
        pane = new ScrollPane(t);
        pane.setScrollingDisabled(true,false);
        pane.setOverscroll(false,false);
        container=new Container<ScrollPane>();
        container.setActor(pane);
        container.fill(1f,0.99f);
        container.setBackground(new NinePatchDrawable(LOG.skin.getPatch("progressbg")));
        actor=container;
    }

    @Override
    public void scale(float ratio) {
        table.getCell(actor).size((int) Math.ceil(size.x*ratio),(int)Math.ceil(size.y*ratio));
        for (Container c: containers) {
            t.getCell(c).width((float) Math.floor(t.getCell(c).getPrefWidth()*ratio));
            t.getCell(c).height((float) Math.floor(t.getCell(c).getPrefHeight()*ratio));
            c.padLeft((float) Math.ceil(c.getPadLeft()*ratio));
            font.getData().setScale(size.x/300*ratio);
        }
    }

    public void addRecipe(final Recipes recipe){
        Item item= recipe.getItem().getItem();
        Label.LabelStyle style= new Label.LabelStyle();
        style.font=font;
        Label lbl= new Label(item.getName(),style);
        Container lblcontainer = new Container<Label>(lbl);
        lblcontainer.setBackground(new NinePatchDrawable(LOG.skin.getPatch("progressbg")));
        lblcontainer.fill(1f,1f);
        lblcontainer.padLeft(size.x*0.01f);

        Image img= new Image(item.getImage());
        Container<Image>imgcontainer= new Container<Image>(img);
        imgcontainer.setBackground(new NinePatchDrawable(LOG.skin.getPatch("progressbg")));
        imgcontainer.fill(0.9f,0.9f);

        t.add(lblcontainer).width(size.x*0.88f).height(size.x*0.12f);
        t.add(imgcontainer).width(size.x*0.12f).height(size.x*0.12f);
        t.row();

        containers.add(lblcontainer,imgcontainer);

        lblcontainer.addListener( new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                craftscene.craftpane.loadrecipe(recipe);

                Process process = null;
                for (Process p:craftscene.getProcesses()) {
                    if (p.getRecipe()==craftscene.craftpane.getRecipe())process=p;
                }
                craftscene.setDisplayed(process);
            }
        } );
    }

}
