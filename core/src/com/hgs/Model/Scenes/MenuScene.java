package com.hgs.Model.Scenes;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.hgs.Model.LOG;
import com.hgs.Model.UI.UIButton;
import com.hgs.Model.UI.UITextTitle;

public class MenuScene extends Scene {
    final UITextTitle titlebar;
    final UIButton farmingbtn;
    final UIButton miningbtn;
    final UIButton exploringbtn;
    final UIButton craftingbtn;
    SceneType lastscene;
    public MenuScene(final LOG log) {
        super(log);
        this.type=SceneType.Menu;
        table.setVisible(false);
        table.setBackground(LOG.skin.getDrawable("pixelblack"));
        titlebar = new UITextTitle(table,"title",new Vector2(LOG.oWIDTH,LOG.oHEIGHT/16*((float)LOG.HEIGHT/LOG.WIDTH)),0.4f);
        NinePatchDrawable btntex = new NinePatchDrawable(LOG.skin.getPatch("progressbg"));
        farmingbtn = new UIButton("Farm",table,btntex,"",new Vector2(LOG.oWIDTH*0.8f,LOG.oHEIGHT/10*((float)LOG.HEIGHT/LOG.WIDTH)),0.5f);
        miningbtn = new UIButton("Mine",table,btntex,"",new Vector2(LOG.oWIDTH*0.8f,LOG.oHEIGHT/10*((float)LOG.HEIGHT/LOG.WIDTH)),0.5f);
        exploringbtn = new UIButton("Explore",table,btntex,"",new Vector2(LOG.oWIDTH*0.8f,LOG.oHEIGHT/10*((float)LOG.HEIGHT/LOG.WIDTH)),0.5f);
        craftingbtn = new UIButton("Craft",table,btntex,"",new Vector2(LOG.oWIDTH*0.8f,LOG.oHEIGHT/10*((float)LOG.HEIGHT/LOG.WIDTH)),0.5f);

        table.top();
        table.add(titlebar.getActor());
        table.row();
        table.add().size(LOG.oHEIGHT/64*((float)LOG.HEIGHT/LOG.WIDTH));
        table.row();
        table.add(farmingbtn.getButton());
        table.row();
        table.add().size(LOG.oHEIGHT/64*((float)LOG.HEIGHT/LOG.WIDTH));
        table.row();
        table.add(miningbtn.getButton());
        table.row();
        table.add().size(LOG.oHEIGHT/64*((float)LOG.HEIGHT/LOG.WIDTH));
        table.row();
        table.add(exploringbtn.getButton());
        table.row();
        table.add().size(LOG.oHEIGHT/64*((float)LOG.HEIGHT/LOG.WIDTH));
        table.row();
        table.add(craftingbtn.getButton());
        table.setDebug(false);
        components.add(titlebar);
        components.add(farmingbtn,miningbtn, exploringbtn, craftingbtn);

        farmingbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                log.setScene(SceneType.Farming);
            }
        });

        miningbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                log.setScene(SceneType.Mining);
            }
        });

        exploringbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                log.setScene(SceneType.Exploring);
            }
        });

        craftingbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                log.setScene(SceneType.Crafting);
            }
        });

        titlebar.getMenubutton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (lastscene==null)return;
                log.setScene(lastscene);
            }
        });

    }

    public void setLastscene(SceneType lastscene) {
        this.lastscene = lastscene;
    }


}
