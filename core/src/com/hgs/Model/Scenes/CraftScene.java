package com.hgs.Model.Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.hgs.Model.Character.Player;
import com.hgs.Model.Character.Skill;
import com.hgs.Model.Inventory.Crafting.Process;
import com.hgs.Model.Inventory.Crafting.Recipes;
import com.hgs.Model.Inventory.Item.Item;
import com.hgs.Model.Inventory.Item.ItemStack;
import com.hgs.Model.Inventory.Item.Items;
import com.hgs.Model.LOG;
import com.hgs.Model.UI.*;

import java.util.HashMap;

public class CraftScene extends Scene {
    final Player player;
    final Skill crafting;
    final UIProgressTitle craftingbar;
    final UILabel moneylabel;
    final UILabel recipelabel;
    final UIRecipePane recipepane;
    public final UICraftPane craftpane;
    final UIButton craftbtn;
    final UIProgressbar craftbar;
    Array<Process>processes;
    Process displayed;
    public CraftScene(LOG log) {
        super(log);
        this.type=SceneType.Crafting;
        table.top();
        craftingbar = new UIProgressTitle(log,type,table,"craftingbar",new Vector2(LOG.oWIDTH,LOG.oHEIGHT/16*((float)LOG.HEIGHT/LOG.WIDTH)), Color.CYAN,0.35f);
        player = log.player;
        crafting = player.getCrafting();
        moneylabel = new UILabel("", Align.center,table,"moneylabel",new Vector2(LOG.oWIDTH/2,LOG.oHEIGHT/8),0.7f*(float)LOG.HEIGHT/LOG.WIDTH);
        recipelabel = new UILabel("Recipes", Align.center,table,"recipelabel",new Vector2(LOG.oWIDTH*0.9f,LOG.oHEIGHT*0.075f*((float)LOG.HEIGHT/LOG.WIDTH)),0.25f*LOG.HEIGHT/LOG.WIDTH);
        NinePatchDrawable patch = new NinePatchDrawable(LOG.skin.getPatch("progressbg"));
        recipelabel.getlabelcontainer().setBackground(patch);
        recipepane = new UIRecipePane(this,table,"recipes",new Vector2(LOG.oWIDTH*0.9f,LOG.oHEIGHT*0.35f*((float)LOG.HEIGHT/LOG.WIDTH)));
        craftpane = new UICraftPane(this,table,"crafting",new Vector2(LOG.oWIDTH*0.9f,LOG.oHEIGHT*0.2f*((float)LOG.HEIGHT/LOG.WIDTH)));
        craftbtn = new UIButton("Craft",table,patch,"craftbtn",new Vector2(LOG.oWIDTH*0.9f,LOG.oHEIGHT*0.1f*((float)LOG.HEIGHT/LOG.WIDTH)),0.5f*LOG.HEIGHT/LOG.WIDTH);
        craftbar = new UIProgressbar(table,"craftbar",new Vector2(LOG.oWIDTH*0.9f,LOG.oHEIGHT*0.03f*((float)LOG.HEIGHT/LOG.WIDTH)),Color.GREEN,1f);
        processes = new Array<Process>();
        table.add(craftingbar.getActor());
        table.row();
        table.add(moneylabel.getActor());
        table.row();
        table.add(recipelabel.getActor());
        table.row();
        table.add(recipepane.getActor());
        table.row();
        table.add(craftpane.getActor());
        table.row();
        table.add(craftbtn.getActor());
        table.row();
        table.add(craftbar.getActor());

        components.add(craftingbar,moneylabel,recipelabel,recipepane);
        components.add(craftpane,craftbtn,craftbar);

        for (Recipes r:Recipes.values()) {
            recipepane.addRecipe(r);
        }

        craftpane.setRecipe(Recipes.values()[0]);

        craftbtn.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Process process = null;
                for (Process p:processes) {
                    if (p.getRecipe()==craftpane.getRecipe())process=p;
                }
                if (process==null&&checksupply()){
                    process= new Process(craftpane.getRecipe());
                    processes.add(process);
                    for (ItemStack stack:process.getRecipe().getMaterial()
                         ) {
                        player.getInventorymodel().RemoveItems(stack.getItem(),stack.getCount());
                    }
                }
                if (process!=null){


                  if (process.progress()== Process.Status.Ended){
                      player.getInventorymodel().AddItems(Items.valueOf(process.getRecipe().getItem().getItem().getName()).getItem(),process.getRecipe().getItem().getCount());

                      double moneyval= process.getRecipe().getMoney();
                      double xpval= process.getRecipe().getXp();
                      double moneyharvest= moneyval*crafting.getmoneymult();
                      double xpharvest=xpval*crafting.getxpmult();

                      player.addbalance(moneyharvest);
                      crafting.addxp(xpharvest);

                      processes.removeValue(process,false);

                      process=null;
                  }
                }
                displayed=process;
                save();
            }

        });

    }

    private void update(float delta) {
        moneylabel.getLabel().setText("[#00ff1a]$"+LOG.formatting.formatdec(player.getbalance()));
        craftingbar.getProgresslabel().setText("Crafting lvl: "+ LOG.formatting.formatdec(crafting.getlevel())+" - EXP: "+LOG.formatting.formatdec(crafting.getxp())+" / "+LOG.formatting.formatdec(crafting.getxpnext()));
        craftingbar.getPb().setValue((float) (crafting.getxp()/crafting.getxpnext()));
        if (displayed!=null) {
            craftbar.getPb().setValue(displayed.getProgress()/displayed.getRecipe().getCraftpoints());
            craftbtn.getButton().setText(String.valueOf(displayed.getStatus()));
            if (displayed.getStatus()==Process.Status.Complete)craftpane.getRewardlabel().setVisible(true);
        }
        else {
            craftbar.getPb().setValue(0);
            craftbtn.getButton().setText("Craft");
            craftpane.getRewardlabel().setVisible(false);
        }

    }

    @Override
    public void render(float delta, Batch batch) {
        super.render(delta, batch);
        update(delta);
    }

    public Player getPlayer() {
        return player;
    }

    public boolean checksupply() {
        for (ItemStack stack : craftpane.getRecipe().getMaterial()) {
            if (!player.getInventorymodel().checkStack(stack.getItem(), stack.getCount())) {
                return false;
            }
        }
        return true;
    }

    public Array<Process> getProcesses() {
        return processes;
    }

    public void setDisplayed(Process displayed) {
        this.displayed = displayed;
    }

    public void save(){
        String craftsave=new String();
        for (Process process:processes
        ) {
            craftsave+=process.getRecipe().getItem().getItem().getName()+","+process.getProgress()+"\n";
        }
        LOG.data.putString("craftsave",craftsave);
        LOG.data.flush();
    }

    public void load(){
        String[] craftdata= LOG.data.getString("craftsave","undefined").split("\n");
        for (String s:craftdata) {
          String[]recipedata= s.split(",");
          if (recipedata.length==2){
              Process p= new Process(Recipes.valueOf(recipedata[0]));
              p.setProgress(Float.parseFloat(recipedata[1]));
              processes.add(p);
              displayed=p;
          }
        }

    }
}

