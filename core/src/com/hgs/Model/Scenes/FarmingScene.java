package com.hgs.Model.Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.hgs.Model.Character.Player;
import com.hgs.Model.Character.Skill;
import com.hgs.Model.Inventory.InventoryModel;
import com.hgs.Model.Inventory.Item.Crop.Crop;
import com.hgs.Model.Inventory.Item.Item;
import com.hgs.Model.Inventory.Item.Items;
import com.hgs.Model.Inventory.Item.Seed.Seed;
import com.hgs.Model.LOG;
import com.hgs.Model.UI.*;

public class FarmingScene extends Scene {
    final Player player;
    final Skill farming;
    final UIProgressTitle farmingxpbar;
    final UILabel moneylabel;
    public final UIFarmArray farmarray;
    final UILabel seedlabel;
    public final UIInventory seedslot;
    private final InventoryModel seedmodel;
    public final UIButton seedinv;
    final UILabel hoelabel;
    public final UIInventory hoeslot;
    private final InventoryModel hoemodel;
    public final UIButton hoeinv;
    Item selected;
    final float itemsize =LOG.WIDTH*0.08f*((float)LOG.HEIGHT/LOG.WIDTH);

    public FarmingScene(final LOG log) {
        super(log);
        this.type=SceneType.Farming;
        table.setVisible(false);
        table.top();
        farmingxpbar= new UIProgressTitle(log,type,table,"farmingxpbar",new Vector2(LOG.oWIDTH,LOG.oHEIGHT/16*((float)LOG.HEIGHT/LOG.WIDTH)), Color.CYAN,0.35f);
        player = log.player;
        farming = player.getFarming();
        moneylabel = new UILabel("", Align.center,table,"moneylabel",new Vector2(LOG.oWIDTH/2,LOG.oHEIGHT/8),0.7f*(float)LOG.HEIGHT/LOG.WIDTH);
        farmarray = new UIFarmArray(log,table,"farm",new Vector2(LOG.oWIDTH*0.5f*((float)LOG.HEIGHT/LOG.WIDTH),LOG.oHEIGHT*0.5f*((float)LOG.HEIGHT/LOG.WIDTH)),20);
        seedlabel = new UILabel("Seed", Align.center,table,"seedlbl",new Vector2(LOG.oWIDTH*0.12f*((float)LOG.HEIGHT/LOG.WIDTH),LOG.oHEIGHT*0.05f*((float)LOG.HEIGHT/LOG.WIDTH)),2f);
        seedslot = new UIInventory(table,"seedlot",new Vector2(LOG.oWIDTH*0.12f*((float)LOG.HEIGHT/LOG.WIDTH),LOG.oHEIGHT*0.12f*((float)LOG.HEIGHT/LOG.WIDTH)),new Vector2(1,1), (int) (LOG.oWIDTH*0.12f*((float)LOG.HEIGHT/LOG.WIDTH)));
        seedinv = new UIButton("",table,LOG.skin.getDrawable("inventory"),"seedinv",new Vector2(LOG.oWIDTH*0.12f*((float)LOG.HEIGHT/LOG.WIDTH),LOG.oHEIGHT*0.05f*((float)LOG.HEIGHT/LOG.WIDTH)),new int[]{LOG.oHEIGHT/30,0,0,0});
        hoelabel = new UILabel("Hoe", Align.center,table,"hoelbl",new Vector2(LOG.oWIDTH*0.12f*((float)LOG.HEIGHT/LOG.WIDTH),LOG.oHEIGHT*0.05f*((float)LOG.HEIGHT/LOG.WIDTH)),2f);
        hoeslot = new UIInventory(table,"hoeslot",new Vector2(LOG.oWIDTH*0.12f*((float)LOG.HEIGHT/LOG.WIDTH),LOG.oHEIGHT*0.12f*((float)LOG.HEIGHT/LOG.WIDTH)),new Vector2(1,1), (int) (LOG.oWIDTH*0.12f*((float)LOG.HEIGHT/LOG.WIDTH)));
        hoeinv = new UIButton("",table,LOG.skin.getDrawable("inventory"),"hoeinv",new Vector2(LOG.oWIDTH*0.12f*((float)LOG.HEIGHT/LOG.WIDTH),LOG.oHEIGHT*0.05f*((float)LOG.HEIGHT/LOG.WIDTH)),new int[]{LOG.oHEIGHT/30,0,0,0});
        table.add(farmingxpbar.getActor()).colspan(2);
        table.row();
        table.add(moneylabel.getlabelcontainer()).colspan(2);
        table.row();
        table.add().height(LOG.HEIGHT*0.025f*((float)LOG.HEIGHT/LOG.WIDTH)).colspan(2);
        table.row();
        table.add(farmarray.getActor()).colspan(2);
        table.row();
        table.add().height(LOG.HEIGHT*0.025f*((float)LOG.HEIGHT/LOG.WIDTH)).colspan(2);
        table.row();
        table.add(seedlabel.getlabelcontainer());
        table.add(hoelabel.getlabelcontainer());
        table.row();
        table.add(seedslot.getActor());
        table.add(hoeslot.getActor());
        table.row();
        table.add(seedinv.getButton());
        table.add(hoeinv.getButton());
        components.add(farmingxpbar);
        components.add(moneylabel, farmarray,seedinv, hoeinv);
        components.add(seedlabel,seedslot, hoelabel, hoeslot);
        seedmodel=player.getSeedmodel();
        hoemodel =player.getHoemodel();
        seedmodel.addInventory(seedslot.getinventory());
        hoemodel.addInventory(hoeslot.getinventory());



        seedslot.getinventory().getSlots().get(0).addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (seedmodel.getStacks().get(0)!=null)
                    selected= seedmodel.getStacks().get(0).getItem();
                return true;
            }
        });

        hoeslot.getinventory().getSlots().get(0).addListener(new ClickListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (hoemodel.getStacks().get(0)!=null)
                    selected= hoemodel.getStacks().get(0).getItem();
                return true;
            }
        });

        seedinv.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                log.itemchoicescene.setMode("Seed");
                log.itemchoicescene.show();
            }
        });

        hoeinv.getButton().addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                log.itemchoicescene.setMode("Hoe");
                log.itemchoicescene.show();
            }
        });

    }

    private void update(float delta){
        farmarray.update(delta);
        if (Gdx.input.isTouched()){
            if (getSelected()!=null) {
                TextureRegion selected= getSelected().getImage();
                String category= getSelected().getData().getString("category","undefined");
                LOG.batch.begin();
                LOG.batch.draw(selected, Gdx.input.getX() - itemsize / 2, LOG.HEIGHT - Gdx.input.getY() - itemsize / 2, itemsize, itemsize);
                LOG.batch.end();
                if (category=="Seed"){
                    Seed selectedseed= (Seed) seedmodel.getStacks().get(0).getItem();
                    if (farmarray.plant(new Vector2(Gdx.input.getX(),Gdx.input.getY()), (Crop) Items.valueOf(selectedseed.getData().getString("crop","undefined")).getItem())){
                        seedmodel.RemoveItems(getSelected(),1);
                        if (seedmodel.getStacks().get(0).getCount()<1)setSelected(null);
                        return;
                    }
                }

                if (category=="Hoe") {

                    if (farmarray.harvest(new Vector2(Gdx.input.getX(),Gdx.input.getY()))){
                        float durability=getSelected().getData().getFloat("durability",-1)-1;
                        getSelected().getData().putFloat("durability",durability);
                        hoemodel.sync();
                        if (durability<=0){
                            hoemodel.RemoveItems(getSelected(),1);
                            setSelected(null);
                        }
                    }
                }

            }
        }else {
            log.farmingscene.setSelected(null);
        }

        moneylabel.getLabel().setText("[#00ff1a]$"+LOG.formatting.formatdec(player.getbalance()));
        farmingxpbar.getProgresslabel().setText("Farming lvl: "+ LOG.formatting.formatdec(farming.getlevel())+" - EXP: "+LOG.formatting.formatdec(farming.getxp())+" / "+LOG.formatting.formatdec(farming.getxpnext()));
        farmingxpbar.getPb().setValue((float) (farming.getxp()/farming.getxpnext()));
    }

    public void hideUI(){
        farmingxpbar.getPb().setVisible(false);
        seedlabel.getlabelcontainer().setVisible(false);
        hoelabel.getlabelcontainer().setVisible(false);
        seedslot.getActor().setVisible(false);
        hoeslot.getActor().setVisible(false);
        seedinv.getButton().setVisible(false);
        hoeinv.getButton().setVisible(false);
    }
    public void showUI(){
        farmingxpbar.getPb().setVisible(true);
        seedlabel.getlabelcontainer().setVisible(true);
        hoelabel.getlabelcontainer().setVisible(true);
        seedslot.getActor().setVisible(true);
        hoeslot.getActor().setVisible(true);
        seedinv.getButton().setVisible(true);
        hoeinv.getButton().setVisible(true);
    }

    public Item getSelected() {
        return selected;
    }

    public void setSelected(Item selected) {
        this.selected = selected;
    }

    @Override
    public void render(float delta, Batch batch) {
        super.render(delta, batch);
        update(delta);
    }
}
